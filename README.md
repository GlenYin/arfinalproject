# README #

Glen Yin 4/29/2021

### What is this repository for? ###

* Augmented Reality Final Project

### How do I get set up? ###

* Build requires Windows 64bit system, located in the Build folder
* Script for the app is located in Assets/OpenCV+Unity/Assets/Scripts/ColorblindEnhancer.cs

### Contact ###
* jyin19@jh.edu
