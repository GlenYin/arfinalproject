﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenCvSharp;


public class ColorblindEnhancer : MonoBehaviour
{
    WebCamTexture _webCamTexture; //webcam variable
    bool modeSwitch; //switch on/off

    // Start is called before the first frame update
    void Start()
    {
        WebCamDevice[] devices = WebCamTexture.devices;

        _webCamTexture = new WebCamTexture(devices[1].name); //device 0 front cam, device 1 back cam
        _webCamTexture.Play();
        modeSwitch = false;
    }

    // Update is called once per frame
    void Update()
    {   
        if(Input.GetKeyDown(KeyCode.F)) //press F to swtich on and off
        {
            if(!modeSwitch)
            {
                modeSwitch = true;
            } else
            {
                modeSwitch = false;
            }
        }
        if(!modeSwitch)
        {
            GetComponent<Renderer>().material.mainTexture = _webCamTexture; // simply display webcam to plane when switch is off
        } else
        {
            Mat img = Capture(); // get a frame from webcam
            Size orig = img.Size();
            Mat downImg = new Mat();
            Cv2.PyrDown(img, downImg); // downsample image (blur then sample) to half its size
            Mat hsvFrame = new Mat();
            Cv2.CvtColor(downImg, hsvFrame, ColorConversionCodes.BGR2HSV); // convert to hsv values
            // set bin borders
            byte[] red_lower = { 140, 120, 50 }; 
            Mat r_lower = new Mat(1, 3, MatType.CV_8UC1, red_lower);
            byte[] red_upper = { 190, 255, 255 };
            Mat r_upper = new Mat(1, 3, MatType.CV_8UC1, red_upper);
            byte[] green_lower = { 20, 70, 80 };
            Mat g_lower = new Mat(1, 3, MatType.CV_8UC1, green_lower);
            byte[] green_upper = { 100, 255, 255 };
            Mat g_upper = new Mat(1, 3, MatType.CV_8UC1, green_upper);
            byte[] blue_lower = { 90, 70, 100 };
            Mat b_lower = new Mat(1, 3, MatType.CV_8UC1, blue_lower);
            byte[] blue_upper = { 130, 255, 220 };
            Mat b_upper = new Mat(1, 3, MatType.CV_8UC1, blue_upper);
            // create masks with only pixels in range
            Mat redMask = new Mat();
            Cv2.InRange(hsvFrame, r_lower, r_upper, redMask);
            Mat greenMask = new Mat();
            Cv2.InRange(hsvFrame, g_lower, g_upper, greenMask);
            Mat blueMask = new Mat();
            Cv2.InRange(hsvFrame, b_lower, b_upper, blueMask);
            // dilate red pixels so noise is cancelled out
            Mat element = new Mat(6, 6, MatType.CV_8UC1, 1);
            Mat dilated_rmask = new Mat();
            Cv2.Dilate(redMask, dilated_rmask, element);
            Mat res_red = new Mat();
            Cv2.BitwiseAnd(downImg, downImg, res_red, dilated_rmask);

            Mat dilated_gmask = new Mat();
            Cv2.Dilate(greenMask, dilated_gmask, element);
            Mat res_green = new Mat();
            Cv2.BitwiseAnd(downImg, downImg, res_green, dilated_gmask);

            Mat dilated_bmask = new Mat();
            Cv2.Dilate(blueMask, dilated_bmask, element);
            Mat res_blue = new Mat();
            Cv2.BitwiseAnd(downImg, downImg, res_blue, dilated_bmask);
            // find contours with cv2 algorithm with each mask
            Point[][] rcontours;
            HierarchyIndex[] rhierarchy;
            Cv2.FindContours(dilated_rmask, out rcontours, out rhierarchy, RetrievalModes.Tree, ContourApproximationModes.ApproxSimple);
            int cntInd = 0;
            Scalar cntRed = new Scalar(0, 79, 153); // colorblindness friendly color coding
            foreach (IEnumerable<Point> contour in rcontours)
            {
                double area = Cv2.ContourArea(contour);
                if (area > 120) // draw contour only if the area of the pixels are larger than 120
                {
                    Cv2.DrawContours(downImg, rcontours, cntInd, cntRed, 10);
                }
                cntInd++;
            }

            Point[][] gcontours;
            HierarchyIndex[] ghierarchy;
            Cv2.FindContours(dilated_gmask, out gcontours, out ghierarchy, RetrievalModes.Tree, ContourApproximationModes.ApproxSimple);
            cntInd = 0;
            Scalar cntGreen = new Scalar(26, 255, 26);
            foreach (IEnumerable<Point> contour in gcontours)
            {
                double area = Cv2.ContourArea(contour);
                if (area > 120)
                {
                    Cv2.DrawContours(downImg, gcontours, cntInd, cntGreen, 10);
                }
                cntInd++;
            }

            Point[][] bcontours;
            HierarchyIndex[] bhierarchy;
            Cv2.FindContours(dilated_bmask, out bcontours, out bhierarchy, RetrievalModes.Tree, ContourApproximationModes.ApproxSimple);
            cntInd = 0;
            Scalar cntBlue = new Scalar(209, 108, 0);
            foreach (IEnumerable<Point> contour in bcontours)
            {
                double area = Cv2.ContourArea(contour);
                if (area > 120)
                {
                    Cv2.DrawContours(downImg, bcontours, cntInd, cntBlue, 10);
                }
                cntInd++;
            }

            // upsample image with gaussian blur (unsample then blur) to original size
            Mat upImg = new Mat();
            Cv2.PyrUp(downImg, upImg, orig);
            // legends for color coding
            Point rTextPos = new Point(1620, 850);
            Cv2.PutText(upImg, "Red", rTextPos, HersheyFonts.HersheySimplex, 2.5f, cntRed, 8);
            Point gTextPos = new Point(1620, 920);
            Cv2.PutText(upImg, "Green", gTextPos, HersheyFonts.HersheySimplex, 2.5f, cntGreen, 8);
            Point bTextPos = new Point(1620, 990);
            Cv2.PutText(upImg, "Blue", bTextPos, HersheyFonts.HersheySimplex, 2.5f, cntBlue, 8);
            // display on plane
            Texture2D NewTexture = OpenCvSharp.Unity.MatToTexture(upImg);
            GetComponent<Renderer>().material.mainTexture = NewTexture;
        }
    }

    public Mat Capture()
    {
        //Initialise the image matrix
        Mat img = new Mat();
        //Grab the frame to the img variable
        img = OpenCvSharp.Unity.TextureToMat(_webCamTexture);

        return img;
    }
}
